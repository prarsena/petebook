name := "Petebook"
organization := "com.bentley"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.6"

libraryDependencies ++= Seq(
  jdbc,
  ehcache,
  ws,
  specs2 % Test,
  guice,
  "io.netty" % "netty-all" % "4.1.52.Final",
  "org.mongodb.scala" %% "mongo-scala-driver" % "4.2.1",
  "com.cloudinary" % "cloudinary-http44" % "1.26.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
)
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.pedrorijo91.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.pedrorijo91.binders._"
