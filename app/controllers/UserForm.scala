package controllers

object UserForm {
  import play.api.data.Forms._
  import play.api.data.Form

  case class UserLoginData(username: String, password: String)
  case class UserRegistrationData(username: String, password: String, posAttribute: String, negAttribute: String)

  val ufLogin = Form(
    mapping(
      "username" -> text,
      "password"  -> text
    )(UserLoginData.apply)(UserLoginData.unapply)
  )

  val ufRegister = Form(
    mapping(
      "username" -> text,
      "password"  -> text,
      "Positive Attribute" -> text,
      "Negative Attribute" -> text
    )(UserRegistrationData.apply)(UserRegistrationData.unapply)
  )

}
