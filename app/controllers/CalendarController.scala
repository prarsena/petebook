package controllers

import models.UserDAO
import play.api._
import play.api.i18n._
import play.api.mvc._

import javax.inject.Inject
import models.db.collections.DbCalendar


//@Singleton
class CalendarController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with I18nSupport {
  import UserForm._
  val appLogger: Logger = Logger("application")


  def allCalendarEntries() = Action { implicit request: Request[AnyContent] =>
    val allEntries = DbCalendar.findAllCalendarEntries().toList
    Ok(views.html.calendar(allEntries))
  }
  /*
  private val postUrl = routes.DatabaseController.addDbUserPost()
  def addDbUser() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.register(uf, postUrl))
  }
  def addDbUserPost() = Action { implicit request: Request[AnyContent] =>
    val formData = uf.bindFromRequest.get
    DbUsers.addUser(formData.username, formData.password)
    Redirect(routes.HomeController.index())
  }
  */

}
