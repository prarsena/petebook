package controllers

import models.UserDAO
import play.api._
import play.api.i18n._
import play.api.mvc._

import javax.inject.Inject
import models.db.collections.DbUsers


//@Singleton
class DatabaseController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with I18nSupport {
  import UserForm._
  val appLogger: Logger = Logger("application")

  def usersOfDb() = Action { implicit request: Request[AnyContent] =>
    val allUsers = DbUsers.findAllUsers().toList
    Ok(views.html.users("Users", allUsers))
  }

  private val postUrl = routes.DatabaseController.addDbUserPost()
  def addDbUser() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.register(ufRegister, postUrl))
  }
  def addDbUserPost() = Action { implicit request: Request[AnyContent] =>
    val formData = ufRegister.bindFromRequest.get
    DbUsers.addUser(formData.username, formData.password,formData.posAttribute, formData.negAttribute)
    Redirect(routes.HomeController.index())
  }


}
