package controllers

import models.{SessionDAO, User, UserDAO}
import controllers.HomeController
import controllers.CalendarController

import javax.inject.Inject
import models.db.collections.{DbCalendar, DbUsers}
import play.api.data._
import play.api.i18n._
import play.api.mvc._

import java.time.{LocalDateTime, ZoneOffset}
import scala.collection._

class UsersController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {
  import UserForm._


  private val postUrl = routes.UsersController.userLoginPost()

  def userLogin() = Action { implicit request =>
    val databaseUsers = DbUsers.findAllUsers()
    Ok(views.html.userlogin(databaseUsers, ufLogin, postUrl))
  }

  def userLoginPost() = Action { implicit request =>
      val formData = ufLogin.bindFromRequest.get
      if (isValidLogin(formData.username, formData.password)) {
        val token = SessionDAO.generateToken(formData.username)
        Redirect(routes.HomeController.index()).withSession(request.session + ("sessionToken" -> token))
      } else {
        Unauthorized(views.html.defaultpages.unauthorized()).withNewSession
      }
  }
  private def isValidLogin(username: String, password: String): Boolean = {
    val validUser = UserDAO.findUser(username)
    UserDAO.getUser(validUser).exists(_.password == password)
  }

  def userHome() = Action { implicit request: Request[AnyContent] =>
    val allEntries = DbCalendar.findAllCalendarEntries()
    withUser(user =>

      Ok(views.html.userhome(user, allEntries)))
  }


  private def withUser[T](block: User => Result)(implicit request: Request[AnyContent]): Result = {
    val user = extractUser(request)

    user
      .map(block)
      .getOrElse(Unauthorized(views.html.defaultpages.unauthorized())) // 401, but 404 could be better from a security point of view
  }
  private def extractUser(req: RequestHeader): Option[User] = {

    val sessionTokenOpt = req.session.get("sessionToken")

    sessionTokenOpt
      .flatMap(token => SessionDAO.getSession(token))
      .filter(_.expiration.isAfter(LocalDateTime.now(ZoneOffset.UTC)))
      .map(_.username)
      .flatMap(UserDAO.getUser)
  }

}
