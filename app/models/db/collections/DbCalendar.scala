package models.db.collections

import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.mongodb.scala.Document
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros.createCodecProvider
import org.mongodb.scala.model.Filters.equal
import models.db.mongo.DataHelpers._
import models.db.mongo.DataStore
import play.api.Logger

import java.text.SimpleDateFormat
import java.util.Date

case class DbCalendar(username: String, CalendarEntry: Option[CalendarEntry])
case class CalendarEntry(date: String, positiveAttribute: Boolean, positiveAmount: Int, negativeAttribute: Boolean, negativeAmount: Int)

object DbCalendar extends DataStore {

  val appLogger: Logger = Logger("application")

  //Required for using Case Classes
  val codecRegistry = fromRegistries(fromProviders(classOf[DbCalendar], (classOf[CalendarEntry]), DEFAULT_CODEC_REGISTRY))

  //Using Case Class to get a collection
  val coll: MongoCollection[DbCalendar] = database.withCodecRegistry(codecRegistry).getCollection("calendar")

  //Using Document to get a collection
  val listings: MongoCollection[Document] = database.getCollection("calendar")

  //Get all records
  def findAllCalendarEntries() = { coll.find().results() }

  def addCalendarEntry(username: String, posAttri: Boolean, posAmount: Int, negAttri: Boolean, negAmount: Int) = {
    val doc: Document = Document (
      "username" -> username,
      "CalendarEntry" -> Document (
        "Date" -> new SimpleDateFormat("yyyy-MM-dd").toString,
        "positiveAttribute" -> posAttri,
        "positiveAmount" -> posAmount,
        "negativeAttribute" -> negAttri,
        "negativeAmount" -> negAmount
      )
    )
    listings.insertOne(doc).printResults("Record Inserted")
    print("Calendar Entry added successfully")
  }
}