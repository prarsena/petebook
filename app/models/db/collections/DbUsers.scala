package models.db.collections

import java.util.Date
import java.util.UUID
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.mongodb.scala.Document
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros.createCodecProvider
import org.mongodb.scala.model.Filters.equal
import models.db.mongo.DataHelpers._
import models.db.mongo.DataStore
import play.api.Logger
import play.api.mvc.Results.Redirect


case class DbUser(username: String, password: String)

object DbUsers extends DataStore {

  val appLogger: Logger = Logger("application")

  //Required for using Case Classes
  val codecRegistry = fromRegistries(fromProviders(classOf[DbUser]), DEFAULT_CODEC_REGISTRY)

  //Using Case Class to get a collection
  val coll: MongoCollection[DbUser] = database.withCodecRegistry(codecRegistry).getCollection("users")

  //Using Document to get a collection
  val listings: MongoCollection[Document] = database.getCollection("users")

  //Get all records
  def findAllUsers() = {
    coll.find().results()
  }

  def findOneUser(uname: String) = {
    val redirectURL = "views.html.userlogin"
    val result = coll.find(equal("username", uname)).headResult()
    if (result != null) {
      result
    } else {

      DbUser("No user", "No user")

    }
  }


  // Insert a new record
  def addUser(username: String, password: String, posAttribute: String, negAttribute: String) = {

    val doc: Document = Document(
      "_id" -> UUID.randomUUID().toString(),
      "username" -> username,
      "password" -> password,
      "positiveAttribute" -> posAttribute,
      "negativeAttribute" -> negAttribute,
      "created" -> new Date()
    )
    listings.insertOne(doc).printResults("Record Inserted")
    print("User added successfully")

  }
}