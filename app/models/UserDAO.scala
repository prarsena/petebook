package models

import scala.collection.mutable
import models.db.collections.DbUsers._

case class User(username: String, password: String)

object UserDAO {


  def returnAllUsers() ={
    val registeredUsers = findAllUsers().toList
    val users = mutable.Map("user001" -> User("user001", "pass001"))
    for (user <- registeredUsers ){
      users += (user.username.toString -> User(user.username, user.password))
    }
    users
  }

  def findUser(username: String) : String = {
    val user = findOneUser(username)
    println(user)
    if (user.username == username){
      println(s"Logged in user: ${user.username}")
      username
    }
    else {
      println("No match")
      "No match"
    }
  }

  def getUser(username: String): Option[User] = {
    val users = returnAllUsers()
    users.get(username)
  }

  def addUser(username: String, password: String): Option[User] = {
    val users = returnAllUsers()
    if(users.contains(username)) {
      Option.empty
    } else {
      val user = User(username, password)
      users.put(username, user)
      Option(user)
    }
  }

}
