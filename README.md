# petebook

Building off of the auth implemented by:
https://pedrorijo.com/blog/scala-play-auth/.


This app adds MongoDB integration, user login, and user registration.

See the conf/application.conf file to set up mongodb info, or follow the steps below.

For some JSON data for your users collection, you can use the `public/javascripts/users.json` and import it to mongodb compass.

Ensure MongoDb is running locally! Create petebook db, and add a document to the `users` collection.

MongoDB shell:
```
> use petebook
> db.users.insert({username: "Peter", password: "mypassword"})
```

Intellij or command line: 
To start the app just type:

```
$ sbt run
```

And open the homepage at [http://localhost:9000/](http://localhost:9000/).

You'll see listed several endpoints, each showing one of the concepts presented in the tutorial:

* Public Page
* User Login Page
* User Registration Page
* Private Page using raw verification
* Private Page using Play helpers 
* Private Page using Custom Actions 
* Login with default credentials
* Logout
* Login with incorrect username and/or password